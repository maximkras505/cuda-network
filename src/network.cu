#include <iostream>
#include <torch/script.h>
#include "cnn_modules.cu"


/* torch:
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=3)
        self.relu1 = nn.ReLU()
        self.maxpool1 = nn.MaxPool2d(2)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3)
        self.relu2 = nn.ReLU()
        self.maxpool2 = nn.MaxPool2d(2)
        self.flatten = nn.Flatten()
        self.fc1 = nn.Linear(1600, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = self.maxpool1(self.relu1(self.conv1(x)))
        x = self.flatten(self.maxpool2(self.relu2(self.conv2(x))))
        x = self.fc2(self.fc1(x))
        return x
*/

void resize_image(
    float *input_image,
    float *output_image,
    int in_w,
    int in_h,
    int out_w,
    int out_h
) {
    float scale_w = (float)in_w / out_w;
    float scale_h = (float)in_h / out_h;

    for (int i = 0; i < out_h; i++) {
        for (int j = 0; j < out_w; j++) {
            int old_i = floor((i + 0.5) * scale_w);
            int old_j = floor((j + 0.5) * scale_h);
            output_image[i * out_w + j] = input_image[old_i * in_w + old_j];
        }
    }

}

class Net
{
public:
    float *conv1, *conv1_bias;
    int64_t *conv1_size, *conv1_bias_size;
    float *gpu_conv1 = NULL, *gpu_conv1_bias = NULL;

    float *conv2, *conv2_bias;
    int64_t *conv2_size, *conv2_bias_size;
    float *gpu_conv2 = NULL, *gpu_conv2_bias = NULL;

    float *fc1, *fc1_bias;
    int64_t *fc1_size, *fc1_bias_size;
    float *gpu_fc1 = NULL, *gpu_fc1_bias = NULL;

    float *fc2, *fc2_bias;
    int64_t *fc2_size, *fc2_bias_size;
    float *gpu_fc2 = NULL, *gpu_fc2_bias = NULL;

    Net(const char *path_to_weights)
    {
        torch::jit::script::Module module = torch::jit::load(path_to_weights);
        for (const torch::jit::NameModule &p : module.named_children())
        {
            if (p.name == "conv1")
            {
                const torch::jit::parameter_list &parameters = p.value.parameters();

                int i = 0;
                for (const torch::jit::detail::ParameterPolicy::value_type &param : parameters)
                {
                    if (i == 0)
                    {
                        conv1_size = (int64_t *)malloc(4 * sizeof(int64_t));
                        std::memcpy(conv1_size, param.sizes().data(), sizeof(int64_t) * 4);
                        int32_t size = conv1_size[0] * conv1_size[1] * conv1_size[2] * conv1_size[3];
                        conv1 = (float *)malloc(size * sizeof(float));
                        std::memcpy(conv1, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    else if (i == 1)
                    {
                        conv1_bias_size = (int64_t *)malloc(1 * sizeof(int64_t));
                        std::memcpy(conv1_bias_size, param.sizes().data(), sizeof(int64_t) * 1);
                        int32_t size = conv1_bias_size[0];
                        conv1_bias = (float *)malloc(size * sizeof(float));
                        std::memcpy(conv1_bias, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    i += 1;
                }
            }

            if (p.name == "conv2")
            {
                const torch::jit::parameter_list &parameters = p.value.parameters();
                int i = 0;
                for (const auto &param : parameters)
                {
                    if (i == 0)
                    {
                        conv2_size = (int64_t *)malloc(4 * sizeof(int64_t));
                        std::memcpy(conv2_size, param.sizes().data(), sizeof(int64_t) * 4);
                        int64_t size = conv2_size[0] * conv2_size[1] * conv2_size[2] * conv2_size[3];
                        conv2 = (float *)malloc(size * sizeof(float));
                        std::memcpy(conv2, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    else if (i == 1)
                    {
                        conv2_bias_size = (int64_t *)malloc(1 * sizeof(int64_t));
                        std::memcpy(conv2_bias_size, param.sizes().data(), sizeof(int64_t) * 1);
                        int64_t size = conv2_bias_size[0];
                        conv2_bias = (float *)malloc(size * sizeof(float));
                        std::memcpy(conv2_bias, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    i += 1;
                }
            }

            if (p.name == "fc1")
            {
                const torch::jit::parameter_list &parameters = p.value.parameters();
                int i = 0;
                for (const auto &param : parameters)
                {
                    if (i == 0)
                    {
                        fc1_size = (int64_t *)malloc(2 * sizeof(int64_t));
                        std::memcpy(fc1_size, param.sizes().data(), sizeof(int64_t) * 2);
                        int64_t size = fc1_size[0] * fc1_size[1];
                        fc1 = (float *)malloc(size * sizeof(float));
                        std::memcpy(fc1, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    else if (i == 1)
                    {
                        fc1_bias_size = (int64_t *)malloc(1 * sizeof(int64_t));
                        std::memcpy(fc1_bias_size, param.sizes().data(), sizeof(int64_t) * 1);
                        int64_t size = fc1_bias_size[0];
                        fc1_bias = (float *)malloc(size * sizeof(float));
                        std::memcpy(fc1_bias, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    i += 1;
                }
            }

            if (p.name == "fc2")
            {
                const torch::jit::parameter_list &parameters = p.value.parameters();
                int i = 0;
                for (const auto &param : parameters)
                {
                    if (i == 0)
                    {
                        fc2_size = (int64_t *)malloc(2 * sizeof(int64_t));
                        std::memcpy(fc2_size, param.sizes().data(), sizeof(int64_t) * 2);
                        int64_t size = fc2_size[0] * fc2_size[1];
                        fc2 = (float *)malloc(size * sizeof(float));
                        std::memcpy(fc2, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    else if (i == 1)
                    {
                        fc2_bias_size = (int64_t *)malloc(1 * sizeof(int64_t));
                        std::memcpy(fc2_bias_size, param.sizes().data(), sizeof(int64_t) * 1);
                        int64_t size = fc2_bias_size[0];
                        fc2_bias = (float *)malloc(size * sizeof(float));
                        std::memcpy(fc2_bias, param.data_ptr<float>(), sizeof(float) * size);
                    }
                    i += 1;
                }
            }
        }
    }

    void to_gpu()
    {
        cudaMalloc((void **)&gpu_conv1, conv1_size[0] * conv1_size[1] * conv1_size[2] * conv1_size[3] * sizeof(*gpu_conv1));
        cudaMemcpy(gpu_conv1, conv1, conv1_size[0] * conv1_size[1] * conv1_size[2] * conv1_size[3] * sizeof(*gpu_conv1), cudaMemcpyHostToDevice);

        cudaMalloc((void **)&gpu_conv1_bias, conv1_bias_size[0] * sizeof(*gpu_conv1_bias));
        cudaMemcpy(gpu_conv1_bias, conv1_bias, conv1_bias_size[0] * sizeof(*gpu_conv1_bias), cudaMemcpyHostToDevice);

        cudaMalloc((void **)&gpu_conv2, conv2_size[0] * conv2_size[1] * conv2_size[2] * conv2_size[3] * sizeof(*gpu_conv2));
        cudaMemcpy(gpu_conv2, conv2, conv2_size[0] * conv2_size[1] * conv2_size[2] * conv2_size[3] * sizeof(*gpu_conv2), cudaMemcpyHostToDevice);

        cudaMalloc((void **)&gpu_conv2_bias, conv2_bias_size[0] * sizeof(*gpu_conv2_bias));
        cudaMemcpy(gpu_conv2_bias, conv2_bias, conv2_bias_size[0] * sizeof(*gpu_conv2_bias), cudaMemcpyHostToDevice);

        cudaMalloc((void **)&gpu_fc1, fc1_size[0] * fc1_size[1] * sizeof(*gpu_fc1));
        cudaMemcpy(gpu_fc1, fc1, fc1_size[0] * fc1_size[1] * sizeof(*gpu_fc1), cudaMemcpyHostToDevice);

        cudaMalloc((void **)&gpu_fc1_bias, fc1_bias_size[0] * sizeof(*gpu_fc1_bias));
        cudaMemcpy(gpu_fc1_bias, fc1_bias, fc1_bias_size[0] * sizeof(*gpu_fc1_bias), cudaMemcpyHostToDevice);

        cudaMalloc((void **)&gpu_fc2, fc2_size[0] * fc2_size[1] * sizeof(*gpu_fc2));
        cudaMemcpy(gpu_fc2, fc2, fc2_size[0] * fc2_size[1] * sizeof(*gpu_fc2), cudaMemcpyHostToDevice);

        cudaMalloc((void **)&gpu_fc2_bias, fc2_bias_size[0] * sizeof(*gpu_fc2_bias));
        cudaMemcpy(gpu_fc2_bias, fc2_bias, fc2_bias_size[0] * sizeof(*gpu_fc2_bias), cudaMemcpyHostToDevice);
    }

    int forward_non_optimized(float *image, int c, int w, int h, bool verbose = false)
    {
        cudaEvent_t start, stop;
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        clock_t begin = clock();

        float *gpu_image = NULL;
        cudaMalloc((void **)&gpu_image, c * w * h * sizeof(*gpu_image));
        cudaMemcpy(gpu_image, image, c * w * h * sizeof(*image), cudaMemcpyHostToDevice);

        cudaEventRecord(start);

        int curr_w = w, curr_h = h, curr_in_c = conv1_size[1], curr_out_c = conv1_size[0];

        // conv1
        dim3 threads_conv1(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_conv1((curr_h + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_conv1_gpu = NULL;
        cudaMalloc((void **)&result_image_conv1_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_conv1_gpu));

        GlobalConv2d<<<grid_conv1, threads_conv1>>>(
            gpu_image,
            result_image_conv1_gpu,
            gpu_conv1,
            gpu_conv1_bias,
            curr_in_c,
            curr_out_c,
            3,
            1,
            curr_w,
            curr_h);

        cudaFree(gpu_image);

        // relu1
        float *result_image_relu1_gpu = NULL;
        cudaMalloc((void **)&result_image_relu1_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_relu1_gpu));

        ReLU_layer<<<grid_conv1, threads_conv1>>>(
            result_image_conv1_gpu,
            result_image_relu1_gpu,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_conv1_gpu);

        // maxpool1
        dim3 threads_mp1(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_mp1((curr_h / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_mp1_gpu = NULL;
        cudaMalloc((void **)&result_image_mp1_gpu, curr_out_c * curr_w / 2 * curr_h / 2 * sizeof(*result_image_mp1_gpu));

        Pool2d<<<grid_mp1, threads_mp1>>>(
            result_image_relu1_gpu,
            result_image_mp1_gpu,
            2,
            2,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_relu1_gpu);

        curr_w = curr_w / 2;
        curr_h = curr_h / 2;
        curr_in_c = conv2_size[1];
        curr_out_c = conv2_size[0];

        // conv2
        dim3 threads_conv2(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_conv2((curr_h + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_conv2_gpu = NULL;
        cudaMalloc((void **)&result_image_conv2_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_conv2_gpu));

        GlobalConv2d<<<grid_conv2, threads_conv2>>>(
            result_image_mp1_gpu,
            result_image_conv2_gpu,
            gpu_conv2,
            gpu_conv2_bias,
            curr_in_c,
            curr_out_c,
            3,
            1,
            curr_w,
            curr_h);

        cudaFree(result_image_mp1_gpu);

        // relu2
        float *result_image_relu2_gpu = NULL;
        cudaMalloc((void **)&result_image_relu2_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_relu2_gpu));

        ReLU_layer<<<grid_conv2, threads_conv2>>>(
            result_image_conv2_gpu,
            result_image_relu2_gpu,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_conv2_gpu);

        // maxpool2
        dim3 threads_mp2(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_mp2((curr_h / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_mp2_gpu = NULL;
        cudaMalloc((void **)&result_image_mp2_gpu, curr_out_c * curr_h / 2 * curr_w / 2 * sizeof(*result_image_mp2_gpu));

        Pool2d<<<grid_mp2, threads_mp2>>>(
            result_image_relu2_gpu,
            result_image_mp2_gpu,
            2,
            2,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_relu2_gpu);

        // float *result_image = (float *)malloc(conv2_size[0] * h * w / 16 * sizeof(float));
        // cudaMemcpy(result_image, result_image_mp2_gpu, conv2_size[0] * h * w / 16 * sizeof(*result_image), cudaMemcpyDeviceToHost);
        // for (int c = 0; c < conv2_size[0]; c++)
        // {
        //     for (int i = 0; i < h / 4; i++)
        //     {
        //         for (int j = 0; j < w / 4; j++)
        //         {
        //             std::cout << std::setw(6) << std::setprecision(4) << result_image[c * w * h / 16 + i * w / 4 + j] << " ";
        //         }
        //         std::cout << std::endl;
        //     }
        //     std::cout << std::endl;
        // }

        // fc1
        float *result_image_fc1_gpu = NULL;
        cudaMalloc((void **)&result_image_fc1_gpu, fc1_size[0] * sizeof(*result_image_fc1_gpu));

        Linear<<<(fc1_size[0] + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(
            result_image_mp2_gpu,
            result_image_fc1_gpu,
            gpu_fc1,
            gpu_fc1_bias,
            fc1_size[1],
            fc1_size[0]);

        cudaFree(result_image_mp2_gpu);

        // fc2
        float *result_image_fc2_gpu = NULL;
        cudaMalloc((void **)&result_image_fc2_gpu, fc2_size[0] * sizeof(*result_image_fc2_gpu));

        Linear<<<(fc2_size[0] + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(
            result_image_fc1_gpu,
            result_image_fc2_gpu,
            gpu_fc2,
            gpu_fc2_bias,
            fc2_size[1],
            fc2_size[0]);

        cudaEventRecord(stop);
        cudaEventSynchronize(stop);

        // extract max index
        float *result_vector = NULL;
        result_vector = (float *)malloc(fc2_size[0] * sizeof(float));
        cudaMemcpy(result_vector, result_image_fc2_gpu, fc2_size[0] * sizeof(*result_vector), cudaMemcpyDeviceToHost);

        cudaFree(result_image_fc2_gpu);

        cudaDeviceSynchronize();
        cudaError_t error = cudaGetLastError();
        if (error != cudaSuccess)
        {
            // print the CUDA error message and exit
            printf("CUDA error: %s\n", cudaGetErrorString(error));
            exit(-1);
        }

        float max_x = result_vector[0];
        int max_i = 0;
        for (int i = 0; i < 10; i++)
        {
            if (result_vector[i] > max_x)
            {
                max_x = result_vector[i];
                max_i = i;
            }
        }

        free(result_vector);

        clock_t end = clock();

        float time_spent_gpu = 0;
        cudaEventElapsedTime(&time_spent_gpu, start, stop);
        time_spent_gpu /= 1000;

        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

        if (verbose)
        {
            printf("Unoptimized forward pass\n");
            printf("Total execution time: %0.6lfs\n", time_spent);
            printf("GPU execution time: %0.6lfs\n", time_spent_gpu);
            printf("------------------------------\n");
        }

        return max_i;
    }

    int forward_relu_optimized(float *image, int c, int w, int h, bool verbose = false)
    {
        cudaEvent_t start, stop;
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        clock_t begin = clock();

        float *gpu_image = NULL;
        cudaMalloc((void **)&gpu_image, c * w * h * sizeof(*gpu_image));
        cudaMemcpy(gpu_image, image, c * w * h * sizeof(*image), cudaMemcpyHostToDevice);

        cudaEventRecord(start);

        int curr_w = w, curr_h = h, curr_in_c = conv1_size[1], curr_out_c = conv1_size[0];

        // conv1
        dim3 threads_conv1(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_conv1((curr_h + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_conv1_gpu = NULL;
        cudaMalloc((void **)&result_image_conv1_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_conv1_gpu));

        GlobalConv2d<<<grid_conv1, threads_conv1>>>(
            gpu_image,
            result_image_conv1_gpu,
            gpu_conv1,
            gpu_conv1_bias,
            curr_in_c,
            curr_out_c,
            3,
            1,
            curr_w,
            curr_h);

        cudaFree(gpu_image);

        // maxpool1
        dim3 threads_mp1(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_mp1((curr_h / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_mp1_gpu = NULL;
        cudaMalloc((void **)&result_image_mp1_gpu, curr_out_c * curr_w / 2 * curr_h / 2 * sizeof(*result_image_mp1_gpu));

        Pool2d<<<grid_mp1, threads_mp1>>>(
            result_image_conv1_gpu,
            result_image_mp1_gpu,
            2,
            2,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_conv1_gpu);

        curr_w = curr_w / 2;
        curr_h = curr_h / 2;
        curr_in_c = conv2_size[1];
        curr_out_c = conv2_size[0];

        // conv2
        dim3 threads_conv2(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_conv2((curr_h + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_conv2_gpu = NULL;
        cudaMalloc((void **)&result_image_conv2_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_conv2_gpu));

        GlobalConv2d<<<grid_conv2, threads_conv2>>>(
            result_image_mp1_gpu,
            result_image_conv2_gpu,
            gpu_conv2,
            gpu_conv2_bias,
            curr_in_c,
            curr_out_c,
            3,
            1,
            curr_w,
            curr_h);

        cudaFree(result_image_mp1_gpu);

        // maxpool2
        dim3 threads_mp2(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_mp2((curr_h / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_mp2_gpu = NULL;
        cudaMalloc((void **)&result_image_mp2_gpu, curr_out_c * curr_h / 2 * curr_w / 2 * sizeof(*result_image_mp2_gpu));

        Pool2d<<<grid_mp2, threads_mp2>>>(
            result_image_conv2_gpu,
            result_image_mp2_gpu,
            2,
            2,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_conv2_gpu);

        // float *result_image = (float *)malloc(conv2_size[0] * h * w / 16 * sizeof(float));
        // cudaMemcpy(result_image, result_image_mp2_gpu, conv2_size[0] * h * w / 16 * sizeof(*result_image), cudaMemcpyDeviceToHost);
        // for (int c = 0; c < conv2_size[0]; c++)
        // {
        //     for (int i = 0; i < h / 4; i++)
        //     {
        //         for (int j = 0; j < w / 4; j++)
        //         {
        //             std::cout << std::setw(6) << std::setprecision(4) << result_image[c * w * h / 16 + i * w / 4 + j] << " ";
        //         }
        //         std::cout << std::endl;
        //     }
        //     std::cout << std::endl;
        // }

        // fc1
        float *result_image_fc1_gpu = NULL;
        cudaMalloc((void **)&result_image_fc1_gpu, fc1_size[0] * sizeof(*result_image_fc1_gpu));

        Linear<<<(fc1_size[0] + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(
            result_image_mp2_gpu,
            result_image_fc1_gpu,
            gpu_fc1,
            gpu_fc1_bias,
            fc1_size[1],
            fc1_size[0]);

        cudaFree(result_image_mp2_gpu);

        // fc2
        float *result_image_fc2_gpu = NULL;
        cudaMalloc((void **)&result_image_fc2_gpu, fc2_size[0] * sizeof(*result_image_fc2_gpu));

        Linear<<<(fc2_size[0] + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(
            result_image_fc1_gpu,
            result_image_fc2_gpu,
            gpu_fc2,
            gpu_fc2_bias,
            fc2_size[1],
            fc2_size[0]);

        cudaEventRecord(stop);
        cudaEventSynchronize(stop);

        // extract max index
        float *result_vector = NULL;
        result_vector = (float *)malloc(fc2_size[0] * sizeof(float));
        cudaMemcpy(result_vector, result_image_fc2_gpu, fc2_size[0] * sizeof(*result_vector), cudaMemcpyDeviceToHost);

        cudaFree(result_image_fc2_gpu);

        cudaDeviceSynchronize();
        cudaError_t error = cudaGetLastError();
        if (error != cudaSuccess)
        {
            // print the CUDA error message and exit
            printf("CUDA error: %s\n", cudaGetErrorString(error));
            exit(-1);
        }

        float max_x = result_vector[0];
        int max_i = 0;
        for (int i = 0; i < 10; i++)
        {
            if (result_vector[i] > max_x)
            {
                max_x = result_vector[i];
                max_i = i;
            }
        }

        free(result_vector);

        clock_t end = clock();

        float time_spent_gpu = 0;
        cudaEventElapsedTime(&time_spent_gpu, start, stop);
        time_spent_gpu /= 1000;

        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

        if (verbose)
        {
            printf("Unoptimized forward pass\n");
            printf("Total execution time: %0.6lfs\n", time_spent);
            printf("GPU execution time: %0.6lfs\n", time_spent_gpu);
            printf("------------------------------\n");
        }

        return max_i;
    }

    int forward_shmem_optimized(float *image, int c, int w, int h, bool verbose = false)
    {
        cudaEvent_t start, stop;
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        clock_t begin = clock();

        float *gpu_image = NULL;
        cudaMalloc((void **)&gpu_image, c * w * h * sizeof(*gpu_image));
        cudaMemcpy(gpu_image, image, c * w * h * sizeof(*image), cudaMemcpyHostToDevice);

        cudaEventRecord(start);

        int curr_w = w, curr_h = h, curr_in_c = conv1_size[1], curr_out_c = conv1_size[0];

        // conv1
        dim3 threads_conv1(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_conv1((curr_h + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_conv1_gpu = NULL;
        cudaMalloc((void **)&result_image_conv1_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_conv1_gpu));

        SharedConv2d<<<grid_conv1, threads_conv1>>>(
            gpu_image,
            result_image_conv1_gpu,
            gpu_conv1,
            gpu_conv1_bias,
            curr_in_c,
            curr_out_c,
            3,
            1,
            curr_w,
            curr_h);

        cudaFree(gpu_image);

        // float *result_image = (float *)malloc(curr_out_c * curr_h * curr_w * sizeof(float));
        // cudaMemcpy(result_image, result_image_conv1_gpu, curr_out_c * curr_h * curr_w * sizeof(*result_image), cudaMemcpyDeviceToHost);
        // for (int c = 0; c < curr_out_c; c++)
        // {
        //     for (int i = 0; i < curr_h; i++)
        //     {
        //         for (int j = 0; j < curr_w; j++)
        //         {
        //             std::cout << std::setw(6) << std::setprecision(4) << result_image[c * curr_h * curr_w + i * curr_w + j] << " ";
        //         }
        //         std::cout << std::endl;
        //     }
        //     std::cout << std::endl;
        // }

        // maxpool1
        dim3 threads_mp1(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_mp1((curr_h / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_mp1_gpu = NULL;
        cudaMalloc((void **)&result_image_mp1_gpu, curr_out_c * curr_w / 2 * curr_h / 2 * sizeof(*result_image_mp1_gpu));

        Pool2d<<<grid_mp1, threads_mp1>>>(
            result_image_conv1_gpu,
            result_image_mp1_gpu,
            2,
            2,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_conv1_gpu);

        curr_w = curr_w / 2;
        curr_h = curr_h / 2;
        curr_in_c = conv2_size[1];
        curr_out_c = conv2_size[0];

        // conv2
        dim3 threads_conv2(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_conv2((curr_h + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_conv2_gpu = NULL;
        cudaMalloc((void **)&result_image_conv2_gpu, curr_out_c * curr_w * curr_h * sizeof(*result_image_conv2_gpu));

        SharedConv2d<<<grid_conv2, threads_conv2>>>(
            result_image_mp1_gpu,
            result_image_conv2_gpu,
            gpu_conv2,
            gpu_conv2_bias,
            curr_in_c,
            curr_out_c,
            3,
            1,
            curr_w,
            curr_h);

        cudaFree(result_image_mp1_gpu);

        // maxpool2
        dim3 threads_mp2(BLOCK_SIZE, BLOCK_SIZE);
        dim3 grid_mp2((curr_h / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE, (curr_w / 2 + BLOCK_SIZE - 1) / BLOCK_SIZE);

        float *result_image_mp2_gpu = NULL;
        cudaMalloc((void **)&result_image_mp2_gpu, curr_out_c * curr_h / 2 * curr_w / 2 * sizeof(*result_image_mp2_gpu));

        Pool2d<<<grid_mp2, threads_mp2>>>(
            result_image_conv2_gpu,
            result_image_mp2_gpu,
            2,
            2,
            curr_out_c,
            curr_w,
            curr_h);

        cudaFree(result_image_conv2_gpu);

        // fc1
        float *result_image_fc1_gpu = NULL;
        cudaMalloc((void **)&result_image_fc1_gpu, fc1_size[0] * sizeof(*result_image_fc1_gpu));

        Linear<<<(fc1_size[0] + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(
            result_image_mp2_gpu,
            result_image_fc1_gpu,
            gpu_fc1,
            gpu_fc1_bias,
            fc1_size[1],
            fc1_size[0]);

        cudaFree(result_image_mp2_gpu);

        // fc2
        float *result_image_fc2_gpu = NULL;
        cudaMalloc((void **)&result_image_fc2_gpu, fc2_size[0] * sizeof(*result_image_fc2_gpu));

        Linear<<<(fc2_size[0] + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(
            result_image_fc1_gpu,
            result_image_fc2_gpu,
            gpu_fc2,
            gpu_fc2_bias,
            fc2_size[1],
            fc2_size[0]);

        cudaEventRecord(stop);
        cudaEventSynchronize(stop);

        // extract max index
        float *result_vector = NULL;
        result_vector = (float *)malloc(fc2_size[0] * sizeof(float));
        cudaMemcpy(result_vector, result_image_fc2_gpu, fc2_size[0] * sizeof(*result_vector), cudaMemcpyDeviceToHost);

        cudaFree(result_image_fc2_gpu);

        cudaDeviceSynchronize();
        cudaError_t error = cudaGetLastError();
        if (error != cudaSuccess)
        {
            // print the CUDA error message and exit
            printf("CUDA error: %s\n", cudaGetErrorString(error));
            exit(-1);
        }

        float max_x = result_vector[0];
        int max_i = 0;
        for (int i = 0; i < 10; i++)
        {
            if (result_vector[i] > max_x)
            {
                max_x = result_vector[i];
                max_i = i;
            }
        }

        free(result_vector);

        clock_t end = clock();

        float time_spent_gpu = 0;
        cudaEventElapsedTime(&time_spent_gpu, start, stop);
        time_spent_gpu /= 1000;

        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

        if (verbose)
        {
            printf("Unoptimized forward pass\n");
            printf("Total execution time: %0.6lfs\n", time_spent);
            printf("GPU execution time: %0.6lfs\n", time_spent_gpu);
            printf("------------------------------\n");
        }

        return max_i;
    }
};
