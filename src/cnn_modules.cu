#include <cuda_runtime.h>

#define BLOCK_SIZE 8
#define MAX_CHANELS 64

__device__ float ReLU(float value)
{
    return value > 0 ? value : 0;
}

__global__ void
GlobalConv2d(
    float *input_tensor,
    float *output_tensor,
    float *weights,
    float *biases,
    int32_t in_channels,
    int32_t out_channels,
    int32_t kernel_size,
    int32_t padding,
    int32_t w,
    int32_t h,
    bool relu_inplace=false)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;

    float tmp = 0;

    if (i < h && j < w && i >= 0 && j >= 0)
    {
        for (int out_c = 0; out_c < out_channels; ++out_c)
        {
            tmp = 0;
            for (int in_c = 0; in_c < in_channels; ++in_c)
            {
                int left_lim_i = (i >= padding) ? -kernel_size / 2 : 0;
                int right_lim_i = (i < h - padding) ? kernel_size / 2 : 0;
                int left_lim_j = (j >= padding) ? -kernel_size / 2 : 0;
                int right_lim_j = (j < w - padding) ? kernel_size / 2 : 0;
                for (int sh_i = left_lim_i; sh_i <= right_lim_i; ++sh_i)
                {
                    for (int sh_j = left_lim_j; sh_j <= right_lim_j; ++sh_j)
                    {
                        tmp += input_tensor[in_c * w * h + (i + sh_i) * w + j + sh_j] *
                               weights[(out_c * in_channels * kernel_size * kernel_size) +
                                       (in_c * kernel_size * kernel_size) +
                                       ((sh_i + padding) * kernel_size) +
                                       (sh_j + padding)];
                    }
                }
            }
            tmp += biases[out_c];
            if (relu_inplace)
                output_tensor[out_c * w * h + i * w + j] = ReLU(tmp);
            else
                output_tensor[out_c * w * h + i * w + j] = tmp;
        }
    }
}

__global__ void
SharedConv2d(
    float *input_tensor,
    float *output_tensor,
    float *weights,
    float *biases,
    int32_t in_channels,
    int32_t out_channels,
    int32_t kernel_size,
    int32_t padding,
    int32_t w,
    int32_t h,
    bool relu_inplace = false)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;

    __shared__ float shared_input[MAX_CHANELS][BLOCK_SIZE + 2][BLOCK_SIZE + 2];

    if (i < h && j < w && i >= 0 && j >= 0) {
        for (int in_c = 0; in_c < in_channels; in_c++)
        {
            if (threadIdx.y == 0)
            {
                shared_input[in_c][threadIdx.y][threadIdx.x + 1] = i > 0 ? input_tensor[in_c * w * h + (i - 1) * w + j] : 0;
            }
            if (threadIdx.y == blockDim.y - 1)
            {
                shared_input[in_c][threadIdx.y + 2][threadIdx.x + 1] = (i < h - 1) ? input_tensor[in_c * w * h + (i + 1) * w + j] : 0;
            }
            if (threadIdx.x == 0)
            {
                shared_input[in_c][threadIdx.y + 1][threadIdx.x] = j > 0 ? input_tensor[in_c * w * h + i * w + (j - 1)] : 0;
                if (threadIdx.y == 0)
                    shared_input[in_c][threadIdx.y][threadIdx.x] = (j > 0 && i > 0) ? input_tensor[in_c * w * h + (i - 1) * w + (j - 1)] : 0;
                if (threadIdx.y == blockDim.y - 1)
                    shared_input[in_c][threadIdx.y + 2][threadIdx.x] = (j > 0 && i < h - 1) ? input_tensor[in_c * w * h + (i + 1) * w + (j - 1)] : 0;
            }
            if (threadIdx.x == blockDim.x - 1)
            {
                shared_input[in_c][threadIdx.y + 1][threadIdx.x + 2] = (j < w - 1) ? input_tensor[in_c * w * h + i * w + (j + 1)] : 0;
                if (threadIdx.y == 0)
                    shared_input[in_c][threadIdx.y][threadIdx.x + 2] = (j < w - 1 && i > 0) ? input_tensor[in_c * w * h + (i - 1) * w + (j + 1)] : 0;
                if (threadIdx.y == blockDim.y - 1)
                    shared_input[in_c][threadIdx.y + 2][threadIdx.x + 2] = (j < w - 1 && i < h - 1) ? input_tensor[in_c * w * h + (i + 1) * w + (j + 1)] : 0;
            }
            

            shared_input[in_c][threadIdx.y + 1][threadIdx.x + 1] = input_tensor[in_c * w * h + i * w + j];
        }
    }

    __syncthreads();

    float tmp = 0;

    if (i < h && j < w && i >= 0 && j >= 0)
    {
        for (int out_c = 0; out_c < out_channels; ++out_c)
        {
            tmp = 0;
            for (int in_c = 0; in_c < in_channels; ++in_c)
            {
                int left_lim_i = -kernel_size / 2;
                int right_lim_i = kernel_size / 2;
                int left_lim_j = -kernel_size / 2;
                int right_lim_j = kernel_size / 2;
                for (int sh_i = left_lim_i; sh_i <= right_lim_i; ++sh_i)
                {
                    for (int sh_j = left_lim_j; sh_j <= right_lim_j; ++sh_j)
                    {
                        tmp += shared_input[in_c][threadIdx.y + sh_i + 1][threadIdx.x + sh_j + 1] *
                               weights[(out_c * in_channels * kernel_size * kernel_size) +
                                       (in_c * kernel_size * kernel_size) +
                                       ((sh_i + 1) * kernel_size) +
                                       (sh_j + 1)];
                    }
                }
            }
            tmp += biases[out_c];
            if (relu_inplace)
                output_tensor[out_c * w * h + i * w + j] = ReLU(tmp);
            else
                output_tensor[out_c * w * h + i * w + j] = tmp;
        }
    }
}

__global__ void
ReLU_layer(
    float *input_tensor,
    float *output_tensor,
    int in_channels,
    int w,
    int h
) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if (i < h && j < w) {
        for (int c = 0; c < in_channels; c++) {
            output_tensor[c * w * h + i * w + j] = ReLU(input_tensor[c * w * h + i * w + j]);
        }
    }
}

__global__ void
Pool2d(
    float *input_tensor,
    float *output_tensor,
    int32_t kernel_size,
    int32_t stride,
    int32_t c,
    int32_t w,
    int32_t h)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    int32_t sc_i = i * stride;
    int32_t sc_j = j * stride;
    int32_t c_i = 0;
    int32_t c_j = 0;
    float cur = 0;
    float max = 0;
    if (sc_i < h && sc_j < w)
    {
        for (int k = 0; k < c; ++k)
        {
            max = -1;
            for (int sh_i = 0; sh_i < kernel_size; ++sh_i)
            {
                for (int sh_j = 0; sh_j < kernel_size; ++sh_j)
                {
                    c_i = sc_i + sh_i;
                    c_j = sc_j + sh_j;
                    if (c_i < h && c_j < w)
                    {
                        cur = input_tensor[(k * w * h) + (c_i * w) + c_j];
                    }
                    else
                    {
                        cur = 0;
                    }
                    if (cur > max)
                    {
                        max = cur;
                    }
                }
            }
            output_tensor[(k * w / stride * h / stride) + (i * w / stride) + j] = max;
        }
    }
}

__global__ void
Linear(
    float *input_tensor,
    float *output_tensor,
    float *weights,
    float *biases,
    int32_t in_units,
    int32_t out_units)
{ 
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < out_units) {
        float tmp = 0;
        for (int j = 0; j < in_units; j++) {
            tmp += weights[i * in_units + j] * input_tensor[j];
        }
        tmp += biases[i];
        output_tensor[i] = tmp;
    }
}
