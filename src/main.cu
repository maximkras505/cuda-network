#include <iostream>


#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image/stb_image_write.h"
#include "network.cu"



int main(int argc, char *argv[])
{
    char *path_to_weights = NULL;
    char *image_name = NULL;
    int conv_version = 0;
    bool benchmark = false;
    int benchmark_times = 100;
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--model_weights") == 0) 
        {
            path_to_weights = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--image_path") == 0)
        {
            image_name = argv[i+1];
            i++;
        }
        else if (strcmp(argv[i], "--conv_version") == 0)
        {
            conv_version = atoi(argv[i+1]);
            i++;
        }
        else if (strcmp(argv[i], "--benchmark") == 0)
        {
            benchmark = true;
            benchmark_times = atoi(argv[i + 1]);
            i++;
        }
        else {
            std::cout << "Unrecognised argument " << argv[i] << std::endl;
            return -1;
        }
    }

    int32_t c, w, h;
    unsigned char *img = stbi_load(image_name, &w, &h, &c, STBI_grey);
    if (img == NULL) {
        std::cout << "Bad image" << std::endl;
        return -1;
    }
    float *image = (float *)malloc(c * w * h * sizeof(float));
    for (int k = 0; k < c; ++k)
    {
        for (int i = 0; i < h; ++i)
        {
            for (int j = 0; j < w; ++j)
            {
                image[k * w * h + i * w + j] = img[(i * w + j) * c + k] / 255.;
            }
        }
    }
    free(img);
    if (w != 28 or h != 28)
    {
        float *new_img = (float *)malloc(28 * 28 * sizeof(float));
        resize_image(image, new_img, w, h, 28, 28);
        free(image);
        image = new_img;
        w = 28;
        h = 28;
    }

    Net net = Net(path_to_weights);
    net.to_gpu();
    
    if (!benchmark) {
        int label = 0;
        if (conv_version == 0)
            label = net.forward_non_optimized(image, c, w, h, true);
        else if (conv_version == 1)
            label = net.forward_relu_optimized(image, c, w, h, true);
        else if (conv_version == 2)
            label = net.forward_shmem_optimized(image, c, w, h, true);
        printf("Your number is: %d\n", label);
    } else {
        clock_t begin = clock();
        for (int i = 0; i < benchmark_times; i++)
        {
            if (conv_version == 0)
                net.forward_non_optimized(image, c, w, h, false);
            else if (conv_version == 1)
                net.forward_relu_optimized(image, c, w, h, false);
            else if (conv_version == 2)
                net.forward_shmem_optimized(image, c, w, h, false);
        }
        clock_t end = clock();
        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC / benchmark_times;
        printf("Mean time: %f\n", time_spent);
    }

    free(image);

    return 0;
}
