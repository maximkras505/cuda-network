## Что сделано
Классификатор чисел (12 баллов)
+ слияние слоёв (+3 балла)
+ использование local shared memory (+5 баллов)

## Обучение и сохранение
Обучал в ноутбуке торчевую модель для классификации мнистовых цифр.
Код модели:
```python
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=3)
        self.relu1 = nn.ReLU()
        self.maxpool1 = nn.MaxPool2d(2)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3)
        self.relu2 = nn.ReLU()
        self.maxpool2 = nn.MaxPool2d(2)
        self.flatten = nn.Flatten()
        self.fc1 = nn.Linear(1600, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = self.maxpool1(self.relu1(self.conv1(x)))
        x = self.flatten(self.maxpool2(self.relu2(self.conv2(x))))
        x = self.fc2(self.fc1(x))
        return x
```

## Структура проекта
./some_images -- несколько изображений мниста \
./models/model.pt -- обученная модель \
./src/main.cu -- основная часть, там считывается изображение и парсятся аргументы, а также если изображение размера >28, то происходит даунскейл до 28. \
./src/network.cu -- код загрузки весов и forward-пассов сетки. \
./src/cnn_modules.cu -- код на cuda для реализации модулей сетки (Conv2d, MaxPool2d, ReLU) \

## Сборка проекта
```sh
cd ./src
mkdir build
cd ./build 
cmake ..
make
./classifier ../../models/model.pt
```

## Запуск проекта
```sh
./classifier --model_weights ../../models/model.pt --conv_version ${model_version} --image_path ${image_path} --benchmark ${n_iters}
--model_weights -- путь до модели, не менять
--image_path -- путь до изображения для классификации
--conv_version -- 0, 1 или 2.
        0 -- обычная неоптимизированная версия.
        1 -- версия, где ReLU встроены в слой конволюции.
        2 -- версия, с использованием local shared memory.
--benchmark -- (optional) число итераций для бенчмарка.
```

## Результаты на моей машине
### Конфигурация ПК:
Intel(R) Core(TM) i5-9400 CPU @ 2.90GHz
NVIDIA GeForce RTX 2060

### Результаты (n_iters=10000)
#### Обычная неоптимизированная версия:
Mean time: 0.001516

#### версия, где ReLU встроены в слой конволюции.
Mean time: 0.001467

#### версия, с использованием local shared memory.
Mean time: 0.001338



